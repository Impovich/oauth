package com.hamza.oauth.beans;

import com.hamza.oauth.model.UserCredentials;
import com.hamza.oauth.utils.HttpUtils;
import com.hamza.oauth.utils.Props;
import org.apache.http.client.utils.URIBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * Created by new on 04.06.2014.
 */
@Named
@ApplicationScoped
public class FbRequest implements Serializable {
    private Properties properties;

    public FbRequest() {
        properties = Props.loadProperties();
    }

    public String getFirstFbRequest() {
        try {
            URI uri = new URIBuilder()
                    .setScheme("https")
                    .setHost("www.facebook.com")
                    .setPath("/dialog/oauth")
                    .setParameter("client_id", "890236467658292")
                    .setParameter("redirect_uri", "http://localhost:8180/OAuth/fb_authorization")
                    .setParameter("scope", "public_profile")
                    .setParameter("response_type", "code")
                    .build();

            return uri.toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }
    }

    public String getSecondFbRequest(String code) {
        try {
            URI uri = new URIBuilder()
                    .setScheme("https")
                    .setHost("graph.facebook.com")
                    .setPath("/oauth/access_token")
                    .setParameter("client_id", "890236467658292")
                    .setParameter("redirect_uri", "http://localhost:8180/OAuth/fb_authorization")
                    .setParameter("client_secret", "59bccec9d0f9b5ae165fbd722b7f7084")
                    .setParameter("code", code)
                    .build();

            return HttpUtils.doGet(uri);
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }
    }

    public String getThirdFbRequest(UserCredentials userCredentials) {
        try {
            URI uri = new URIBuilder()
                    .setScheme("https")
                    .setHost("graph.facebook.com")
                    .setPath("/me")
                    .setParameter("access_token", userCredentials.getAccess_token())
                    .build();

            return HttpUtils.doGet(uri);
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }
    }
}
