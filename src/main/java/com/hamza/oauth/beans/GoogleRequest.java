package com.hamza.oauth.beans;

import com.hamza.oauth.model.UserCredentials;
import com.hamza.oauth.utils.HttpUtils;
import com.hamza.oauth.utils.Props;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by new on 04.06.2014.
 */
@Named
@ApplicationScoped
public class GoogleRequest {
    private Properties properties;

    public GoogleRequest() {
        properties = Props.loadProperties();
    }

    public String getFirstGoogleRequest() {
        try {
            URI uri = new URIBuilder()
                    .setScheme("https")
                    .setHost("accounts.google.com")
                    .setPath("/o/oauth2/auth")
                    .setParameter("client_id", "1000782757796-c2tfhn5gbrbtob65sg8ucieesa57gbdk.apps.googleusercontent.com")
                    .setParameter("scope", "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile")
                    .setParameter("redirect_uri", "http://localhost:8180/OAuth/google_authorization")
                    .setParameter("response_type", "code")
                    .build();

            return uri.toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }
    }

    public String getSecondGoogleRequest(String code) {
        try {
            URI uri = new URIBuilder()
                    .setScheme("https")
                    .setHost("accounts.google.com")
                    .setPath("/o/oauth2/token")
                    .build();

            List<NameValuePair> nameValuePairs = new ArrayList<>(1);
            nameValuePairs.add(new BasicNameValuePair("grant_type",
                    "authorization_code"));
            nameValuePairs.add(new BasicNameValuePair("code", code));
            nameValuePairs.add(new BasicNameValuePair("client_id", "1000782757796-c2tfhn5gbrbtob65sg8ucieesa57gbdk.apps.googleusercontent.com"));
            nameValuePairs.add(new BasicNameValuePair("client_secret", "bJ_xZhj_8speT8gm_mniwxgN"));
            nameValuePairs.add(new BasicNameValuePair("redirect_uri", "http://localhost:8180/OAuth/google_authorization"));

            return HttpUtils.doPost(uri, nameValuePairs);
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }

    }

    public String getThirdGoogleRequest(UserCredentials userCredentials) {
        try {
            URI uri = new URIBuilder()
                    .setScheme("https")
                    .setHost("www.googleapis.com")
                    .setPath("/oauth2/v1/userinfo")
                    .setParameter("access_token", userCredentials.getAccess_token())
                    .build();

            return HttpUtils.doGet(uri);
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }

    }

}
