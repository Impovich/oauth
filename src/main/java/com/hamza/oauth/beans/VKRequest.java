package com.hamza.oauth.beans;

import com.hamza.oauth.model.UserCredentials;
import com.hamza.oauth.utils.HttpUtils;
import com.hamza.oauth.utils.Props;
import org.apache.http.client.utils.URIBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * Created by Impovich on 3.6.2014.
 */
@Named
@ApplicationScoped
public class VkRequest implements Serializable {
    private Properties properties;

    public VkRequest() {
        properties = Props.loadProperties();

    }

    public String getFirstVKRequest() {
        try {
            URI uri = new URIBuilder()
                    .setScheme("http")
                    .setHost("oauth.vk.com")
                    .setPath("/authorize")
                    .setParameter("client_id", properties.getProperty("vkClientId"))
                    .setParameter("scope", properties.getProperty("vkScope"))
                    .setParameter("redirect_uri", properties.getProperty("vkRedirectUri"))
                    .build();

            return uri.toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }
    }

    public String getSecondVKRequest(String code) {
        try {
            URI uri = new URIBuilder()
                    .setScheme("https")
                    .setHost("oauth.vk.com")
                    .setPath("/access_token")
                    .setParameter("client_id", properties.getProperty("vkClientId"))
                    .setParameter("client_secret", properties.getProperty("vkSecret"))
                    .setParameter("code", code)
                    .setParameter("redirect_uri", properties.getProperty("vkRedirectUri"))
                    .build();

            return HttpUtils.doGet(uri);
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }

    }

    public String getThirdVKRequest(UserCredentials userCredentials) {
        try {
            URI uri = new URIBuilder()
                    .setScheme("https")
                    .setHost("api.vk.com")
                    .setPath("/method/users.get")
                    .setParameter("user_ids", userCredentials.getUser_id())
                    .setParameter("fields", properties.getProperty("vkFields"))
                    .setParameter("v", properties.getProperty("vkApiVersion"))
                    .build();

            return HttpUtils.doGet(uri).replace("{\"response\":[", "").replace("}]}", "}");
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }

    }

}
