package com.hamza.oauth.beans;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.hamza.oauth.model.UserCredentials;
import com.hamza.oauth.utils.HttpUtils;
import com.hamza.oauth.utils.Props;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by new on 16.06.2014.
 */
@Named
@ApplicationScoped
public class OdkRequest implements Serializable {
    private Properties properties;

    public OdkRequest() {
        properties = Props.loadProperties();
    }

    public String getFirstODKRequest() {
        try {
            URI uri = new URIBuilder()
                    .setScheme("http")
                    .setHost("odnoklassniki.ru")
                    .setPath("/oauth/authorize")
                    .setParameter("client_id", "1092927488")
                    .setParameter("response_type", "code")
                    .setParameter("redirect_uri", "http://localhost:8180/OAuth/odk_authorization")
                    .setParameter("scope", "EMAIL")
                    .build();

            return uri.toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }
    }

    public String getSecondODKRequest(String code) {
        try {
            URI uri = new URIBuilder()
                    .setScheme("http")
                    .setHost("api.odnoklassniki.ru")
                    .setPath("/oauth/token.do")
                    .build();

            List<NameValuePair> nameValuePairs = new ArrayList<>(1);
            nameValuePairs.add(new BasicNameValuePair("grant_type",
                    "authorization_code"));
            nameValuePairs.add(new BasicNameValuePair("code", code));
            nameValuePairs.add(new BasicNameValuePair("client_id", "1092927488"));
            nameValuePairs.add(new BasicNameValuePair("client_secret", "BB1B1AD48F6416E02BD34D45"));
            nameValuePairs.add(new BasicNameValuePair("redirect_uri", "http://localhost:8180/OAuth/odk_authorization"));

            return HttpUtils.doPost(uri, nameValuePairs);
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }
    }

    public String getThirdODKRequest(UserCredentials userCredentials) {
        String tokenAndSecretToMD5 = userCredentials.getAccess_token() + "BB1B1AD48F6416E02BD34D45";
        String sig = "application_key=CBABMFCCEBABABABA" + "method=users.getCurrentUser" + generateMD5(tokenAndSecretToMD5);

        try {
            URI uri = new URIBuilder()
                    .setScheme("http")
                    .setHost("api.odnoklassniki.ru")
                    .setPath("/fb.do")
                    .setParameter("method", "users.getCurrentUser")
                    .setParameter("access_token", userCredentials.getAccess_token())
                    .setParameter("application_key", "CBABMFCCEBABABABA")
                    .setParameter("sig", generateMD5(sig))
                    .build();

            return HttpUtils.doGet(uri);
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }

    }

    private String generateMD5(String toMD5) {
        Hasher hasher = Hashing.md5().newHasher();
        hasher.putString(toMD5, Charset.forName("UTF-8"));
        return hasher.hash().toString();
    }

}
