package com.hamza.oauth.model.odk;

/**
 * Created by Impovich on 17.6.2014.
 */
public class Location {
    private String countryCode;
    private String country;
    private String city;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
