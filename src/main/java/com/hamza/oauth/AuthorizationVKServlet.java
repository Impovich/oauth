package com.hamza.oauth;

import com.google.gson.Gson;
import com.hamza.oauth.beans.VkRequest;
import com.hamza.oauth.model.UserCredentials;
import com.hamza.oauth.model.UserProfile;
import com.hamza.oauth.model.vk.VkUserProfile;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Created by Impovich on 1.6.2014.
 */

@WebServlet(value = "/vk_authorization", name = "vk-authorization-servlet")
public class AuthorizationVKServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(AuthorizationVKServlet.class.getName());
    @Inject
    private UserCredentials userCredentials;
    @Inject
    private VkUserProfile vkUserProfile;
    @Inject
    private VkRequest vkRequest;
    @Inject
    private UserProfile userProfile;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String baseInfoOfUser = null;

        String code = request.getParameter("code");
        if (!isNullOrEmpty(code)) {
            baseInfoOfUser = vkRequest.getSecondVKRequest(code);
            logger.info(baseInfoOfUser);
        }

        if (!isNullOrEmpty(baseInfoOfUser)) {
            Gson gson = new Gson();
            userCredentials = gson.fromJson(baseInfoOfUser, UserCredentials.class);

            userProfile.setUserId(userCredentials.getUser_id());
            userProfile.setEmail(userCredentials.getEmail());

            String fullInfoOfUser = vkRequest.getThirdVKRequest(userCredentials);
            logger.info(fullInfoOfUser);

            if (!isNullOrEmpty(fullInfoOfUser)) {
                vkUserProfile = gson.fromJson(fullInfoOfUser, VkUserProfile.class);
                userProfile.setPhoto(vkUserProfile.getPhoto_max_orig());
                userProfile.setFirstName(vkUserProfile.getFirst_name());
                userProfile.setLastName(vkUserProfile.getLast_name());
                userProfile.setCountry(vkUserProfile.getCountry());
                userProfile.setCity(vkUserProfile.getCity());
                userProfile.setSex(vkUserProfile.getSex());
            }



            response.sendRedirect("/OAuth/faces/welcome.xhtml");
        }

    }
}
