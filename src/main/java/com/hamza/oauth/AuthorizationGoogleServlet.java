package com.hamza.oauth;

import com.google.gson.Gson;
import com.hamza.oauth.beans.GoogleRequest;
import com.hamza.oauth.model.google.GoogleUserProfile;
import com.hamza.oauth.model.UserCredentials;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Created by Impovich on 9.6.2014.
 */
@WebServlet(value = "/google_authorization", name = "google-authorization-servlet")
public class AuthorizationGoogleServlet extends HttpServlet {
    private static Logger logger = Logger.getLogger(AuthorizationGoogleServlet.class.getName());
    @Inject
    private UserCredentials userCredentials;
    @Inject
    private GoogleUserProfile googleUserProfile;
    @Inject
    private GoogleRequest googleRequest;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String baseInfoOfUser = null;

        String code = request.getParameter("code");
        if (!isNullOrEmpty(code)) {
            baseInfoOfUser = googleRequest.getSecondGoogleRequest(code);
            logger.info(baseInfoOfUser);
        }

        if (!isNullOrEmpty(baseInfoOfUser)) {
            Gson gson = new Gson();
            userCredentials = gson.fromJson(baseInfoOfUser, UserCredentials.class);

            String fullInfoOfUser = googleRequest.getThirdGoogleRequest(userCredentials);
            logger.info(fullInfoOfUser);

            if (!isNullOrEmpty(fullInfoOfUser)) {
                googleUserProfile = gson.fromJson(fullInfoOfUser, GoogleUserProfile.class);
            }

            response.sendRedirect("/OAuth/faces/welcome.xhtml");
        }

    }
}
