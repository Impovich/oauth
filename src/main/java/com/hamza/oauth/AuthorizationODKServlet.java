package com.hamza.oauth;

import com.google.gson.Gson;
import com.hamza.oauth.beans.OdkRequest;
import com.hamza.oauth.model.odk.OdkUserProfile;
import com.hamza.oauth.model.UserCredentials;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Created by new on 16.06.2014.
 */
@WebServlet(value = "/odk_authorization", name = "odk-authorization-servlet")
public class AuthorizationODKServlet extends HttpServlet {
    private static Logger logger = Logger.getLogger(AuthorizationODKServlet.class.getName());
    @Inject
    private UserCredentials userCredentials;
    @Inject
    private OdkUserProfile odkUserProfile;
    @Inject
    private OdkRequest odkRequest;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String baseInfoOfUser = null;

        String code = request.getParameter("code");
        if (!isNullOrEmpty(code)) {
            baseInfoOfUser = odkRequest.getSecondODKRequest(code);
            logger.info(baseInfoOfUser);
        }

        if (!isNullOrEmpty(baseInfoOfUser)) {
            Gson gson = new Gson();
            userCredentials = gson.fromJson(baseInfoOfUser, UserCredentials.class);

            String fullInfoOfUser = odkRequest.getThirdODKRequest(userCredentials);
            logger.info(fullInfoOfUser);

            if (!isNullOrEmpty(fullInfoOfUser)) {
                odkUserProfile = gson.fromJson(fullInfoOfUser, OdkUserProfile.class);
            }
            response.sendRedirect("/OAuth/faces/welcome.xhtml");
        }

    }
}
