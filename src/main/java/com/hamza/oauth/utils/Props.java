package com.hamza.oauth.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Impovich on 5.6.2014.
 */
public class Props {

    public static Properties properties;

    private Props() {
    }

    public static Properties loadProperties() {
        if (properties == null) {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream inputStream = loader.getResourceAsStream("/oauth.properties");
            properties = new Properties();
            try {
                properties.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return properties;
        }
        return properties;
    }
}
