package com.hamza.oauth;

import com.google.common.base.Splitter;
import com.google.gson.Gson;
import com.hamza.oauth.beans.FbRequest;
import com.hamza.oauth.model.fb.FbUserProfile;
import com.hamza.oauth.model.UserCredentials;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Created by Impovich on 9.6.2014.
 */
@WebServlet(value = "/fb_authorization", name = "fb-authorization-servlet")
public class AuthorizationFbServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(AuthorizationFbServlet.class.getName());
    @Inject
    private UserCredentials userCredentials;
    @Inject
    private FbUserProfile fbUserProfile;
    @Inject
    private FbRequest fbRequest;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String baseInfoOfUser = null;
        String code = request.getParameter("code");

        if (!isNullOrEmpty(request.getParameter("code"))) {
            baseInfoOfUser = fbRequest.getSecondFbRequest(code);
            logger.info(baseInfoOfUser);
        }

        if (!isNullOrEmpty(baseInfoOfUser)) {
            Map<String, String> params = parse(baseInfoOfUser);
            userCredentials.setAccess_token(params.get("access_token"));
            userCredentials.setExpires_in(Integer.parseInt(params.get("expires")));

            String fullInfoOfUser = fbRequest.getThirdFbRequest(userCredentials);
            logger.info(fullInfoOfUser);

            if (!isNullOrEmpty(fullInfoOfUser)) {
                fbUserProfile = new Gson().fromJson(fullInfoOfUser, FbUserProfile.class);
            }

            response.sendRedirect("/OAuth/faces/welcome.xhtml");
        }

    }

    private Map<String, String> parse(String baseInfoOfUser) {
        return Splitter.on('&').withKeyValueSeparator("=").split(baseInfoOfUser);
    }
}
